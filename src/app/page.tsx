import MaxWidthWrapper from "@/components/MaxWithWrapper";
import { buttonVariants } from "@/components/ui/button";
import { ArrowRight } from "lucide-react";
import Link from "next/link";

export default function Home() {
  return (
    <>
      <MaxWidthWrapper className="mb-20 mt-16 sm:mt-32 flex flex-col items-center justify-center text-center">
        <h1 className="max-w-4xl text-4xl font-bold md:text-5xl lg:text-6xl text-gray-900">
          Optimiza tus <span className="text-[--lime-green]">ingresos</span> de
          ventas con una administración sin esfuerzo.
        </h1>
        <p className="mt-5 max-w-prose text-black sm:text-lg">
          Lleva un registro de los ingresos de tu negocio con facilidad, gracias
          a{" "}
          <span className="font-semibold text-[--lime-green]">
            income-book.
          </span>
        </p>
        <Link
          className={buttonVariants({
            size: "lg",
            className: "mt-5 btn",
          })}
          href="/"
        >
          Empezar <ArrowRight className="ml-2 h-5 w-5" />
        </Link>
      </MaxWidthWrapper>
    </>
  );
}
