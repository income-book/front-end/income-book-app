import type { Metadata } from "next";
import { Outfit } from "next/font/google";
import "./globals.css";
import { cn } from "@/lib/utils";
import Navbar from "@/components/Navbar";

const outfit = Outfit({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "income-book",
  description: "income book app",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en" className="ligth">
      <body className={cn("min-h-screen antialiased grainy", outfit.className)}>
        <Navbar />
        {children}
      </body>
    </html>
  );
}
