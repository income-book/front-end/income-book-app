import MaxWidthWrapper from "@/components/MaxWithWrapper";
import { buttonVariants } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { LogIn } from "lucide-react";
import Link from "next/link";

export default function Page() {
  return (
    <main>
      <MaxWidthWrapper className="mb-20 mt-16 sm:mt-32 flex flex-col items-center justify-center">
        <article className="w-full max-w-md bg-white shadow-md">
          <form className="rounded p-5">
            <div className="mb-6">
              <Label htmlFor="email">correo electrónico</Label>
              <Input id="email" name="email" type="email" />
            </div>
            <div className="mb-6">
              <Label htmlFor="password">contraseña</Label>
              <Input id="password" name="password" type="password" />
            </div>
            <div className="flex items-center justify-between">
              <button
                type="submit"
                className={buttonVariants({
                  variant: "default",
                  size: "default",
                  className: "w-full h-12",
                })}
              >
                Continuar
                <LogIn className="ml-2 h-5 w-5" />
              </button>
            </div>
          </form>
          <footer className="px-5 py-3">
            <Link
              className={buttonVariants({
                variant: "secondary",
                size: "default",
                className: "w-full h-12",
              })}
              href="/login"
            >
              <span className="mr-1.5 w-9 h-9">
                <svg
                  className=""
                  fill="none"
                  aria-hidden="true"
                  focusable="false"
                  viewBox="0 0 32 32"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fill="#4285F4"
                    d="M30.363 16.337c0-.987-.088-1.925-.238-2.837H16v5.637h8.087c-.362 1.85-1.424 3.413-3 4.476v3.75h4.826c2.825-2.613 4.45-6.463 4.45-11.026Z"
                  ></path>
                  <path
                    fill="#34A853"
                    d="M16 31c4.05 0 7.438-1.35 9.913-3.637l-4.826-3.75c-1.35.9-3.062 1.45-5.087 1.45-3.912 0-7.225-2.638-8.413-6.2H2.612v3.862C5.075 27.625 10.137 31 16 31Z"
                  ></path>
                  <path
                    fill="#FBBC05"
                    d="M7.588 18.863A8.704 8.704 0 0 1 7.112 16c0-1 .175-1.963.476-2.863V9.275H2.612a14.826 14.826 0 0 0 0 13.45l4.976-3.863Z"
                  ></path>
                  <path
                    fill="#EA4335"
                    d="M16 6.938c2.212 0 4.188.762 5.75 2.25l4.275-4.276C23.438 2.487 20.05 1 16 1 10.137 1 5.075 4.375 2.612 9.275l4.975 3.862c1.188-3.562 4.5-6.2 8.413-6.2Z"
                  ></path>
                </svg>
              </span>
              <span>Continuar con Google</span>
            </Link>
            <p className="pt-2 text-sm m-auto">
              ¿Aún no tiene cuenta?{" "}
              <Link className="text-[--lime-green]" href="/signup">
                Regístrese aquí.
              </Link>
            </p>
          </footer>
        </article>
      </MaxWidthWrapper>
    </main>
  );
}
